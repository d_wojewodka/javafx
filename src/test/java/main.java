import java.nio.charset.StandardCharsets;
import static java.nio.file.StandardOpenOption.*;

public class main {
    public static void main(String[] args) {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        System.out.printf("%n[Password] *********************************************%n");

        Password pwd = new Password("4}xYqB#!&eGDqtkxK!fh]^$ZAu8#}n$<BBxkzV{Su+@G8n'*W6&BpQYxm)ra#)wsr`ft?+}YpAnjY");
        Integer pwdTest = 1000;
        Integer pwdCounter = pwdTest;

        for (int i=0; i<pwdTest; i++) {
            pwd.encryptPassword();

            if (!pwd.validatePassword("4}xYqB#!&eGDqtkxK!fh]^$ZAu8#}n$<BBxkzV{Su+@G8n'*W6&BpQYxm)ra#)wsr`ft?+}YpAnjY")) {
                pwdCounter--;
            }
        }

        System.out.printf("\t[%d / %d] test passed successfully%n", pwdCounter, pwdTest);

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        System.out.printf("%n[FileInOut] *********************************************%n");

        FileIO fileIO = new FileIO("testFileIO.txt");
        fileIO.setCharset(StandardCharsets.US_ASCII);
        fileIO.setContent(String.format("[%d] bladlasjfasjasjgf%n", fileIO.getLineCount()), WRITE, APPEND, CREATE);
        System.out.printf("\t%s%n", fileIO.getContentByLine(fileIO.getLineCount()));
        fileIO.setContent("");
        System.out.printf("\t%s%n", fileIO.getContent());

        Integer fileTest = 25;
        Integer fileCounter = fileTest;

        for (int i=0; i<=fileTest; i++) {
            fileIO.setContent(String.format("[%d] bladlasjfasjasjgf\n", fileIO.getLineCount()+1), WRITE, APPEND, CREATE);
        }
        System.out.printf("\t%s%n", fileIO.getContent());
        fileIO.removeContentByLine(3);
        //fileIO.delete();
    }
}
