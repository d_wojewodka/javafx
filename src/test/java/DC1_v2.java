import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DC1_v2 {
    // REGAŁ, PÓŁKI, PRZĘSŁA, SZEROKOŚĆ_PRZĘSŁA, PARA
    private TreeMap<Integer, List<Integer>> schemat = new TreeMap<Integer, List<Integer>>();
    private List<String> shelves = new ArrayList<>();
    String table = "MAGAZYN\t" +
            "STREFA\t" +
            "REGAŁ\t" +
            "PÓŁKA\t" +
            "PRZĘSŁO\t" +
            "WEJŚCIE\t" +
            "PARA\t" +
            "ERP" +
            System.lineSeparator();

    private void initialize() {
        shelves.add("A");
        shelves.add("B");
        shelves.add("C");
        shelves.add("D");

        // REGAŁ, PÓŁKI, PRZĘSŁA, SZEROKOŚĆ_PRZĘSŁA, PARA
        schemat.put(0, getList(1, 3, 14, 3, 1));
        schemat.put(1, getList(2, 3, 14, 3, 1));

        schemat.put(2, getList(3, 3, 14, 3, 2));

        schemat.put(3, getList(4, 2, 14, 3, 3));
        schemat.put(4, getList(5, 2, 14, 3, 3));

        schemat.put(5, getList(6, 2, 11, 4, 4));
        schemat.put(6, getList(7, 2, 11, 4, 4));

        schemat.put(7, getList(8, 2, 11, 4, 5));
        schemat.put(8, getList(9, 2, 11, 4, 5));

        schemat.put(9, getList(10, 4, 11, 4, 6));
        schemat.put(10, getList(11, 4, 11, 4, 6));

        schemat.put(11, getList(12, 4, 11, 4, 7));
        schemat.put(12, getList(13, 4, 11, 4, 7));

        schemat.put(13, getList(14, 4, 11, 4, 8));
        schemat.put(14, getList(15, 4, 11, 4, 8));

        schemat.put(15, getList(16, 4, 11, 4, 9));

        schemat.put(16, getList(17, 4, 11, 4, 10));
        schemat.put(17, getList(18, 4, 11, 4, 10));

        schemat.put(18, getList(19, 4, 11, 4, 11));
        schemat.put(19, getList(20, 4, 11, 4, 11));

        schemat.put(20, getList(21, 4, 11, 4, 12));
        schemat.put(21, getList(22, 4, 11, 4, 12));

        schemat.put(22, getList(23, 4, 11, 4, 13));
        schemat.put(23, getList(24, 2, 11, 4, 13));

        schemat.put(24, getList(25, 2, 11, 4, 14));

        schemat.put(25, getList(26, 2, 11, 4, 15));
        schemat.put(26, getList(27, 2, 11, 4, 15));

        schemat.put(27, getList(28, 2, 11, 4, 16));
        schemat.put(28, getList(29, 2, 11, 4, 16));

        schemat.put(29, getList(30, 2, 11, 4, 17));
        schemat.put(30, getList(31, 2, 11, 4, 17));

        schemat.put(31, getList(32, 2, 11, 4, 18));

        schemat.put(32, getList(33, 2, 11, 4, 19));
        schemat.put(33, getList(34, 2, 11, 4, 19));

        schemat.put(34, getList(35, 2, 11, 4, 20));
        schemat.put(35, getList(36, 2, 11, 4, 20));

        schemat.put(36, getList(37, 1, 11, 4, 21));

        schemat.put(37, getList(38, 1, 11, 4, 22));
        schemat.put(38, getList(39, 2, 11, 4, 22));

        schemat.put(39, getList(40, 2, 11, 4, 23));
    }

    public static void main(String[] args) {
        DC1_v2 start = new DC1_v2();
        start.initialize();
        start.generate();
    }

    private List<Integer> getList(Integer regal, Integer polki, Integer przeslo, Integer szerokosc_przesla, Integer para) {
        List<Integer> list = new ArrayList();
        list.add(regal);
        list.add(polki);
        list.add(przeslo);
        list.add(szerokosc_przesla);
        list.add(para);

        return list;
    }

    public String calculateRow(Integer rack, Integer shelf, Integer span, Integer para, String erp) {
        String row = "";

        row += "DC1\t";
        row += "B0\t";
        row += String.format("R%s\t", rack > 10 ? rack : String.format("0%s", rack));
        row += shelves.get(shelf) + "\t";
        row += span + "\t";
        row += String.format("W%d\t", rack);
        row += String.format("P%d\t", para);
        row += erp + "\t";
        row += System.lineSeparator();

        return row;
    }

    public String calculateErp(Integer a, Integer b, Integer c) {
        return String.format("S%d_G/%d/%d", a, b, c);
    }

    private void generate() {
        for (Map.Entry<Integer, List<Integer>> entry : schemat.entrySet()) {
            List<Integer> value = entry.getValue();

            for (Integer shelf = 0; shelf<value.get(1); shelf++) { //półka
                Integer idx = 1;
                for (Integer span = 1; span<(value.get(2) * value.get(3) + 1); span++) { //przęsła * naklejki na przęsło
                    if (span % 3 == 0) {
                        idx++;
                    }
                    String erp = String.format("S%d_G/%d/%d", value.get(4), shelf, idx);
                    System.out.printf(calculateRow(value.get(0), shelf, span, value.get(4), erp));
                }
            }
        }
    }
}
