import java.nio.file.StandardOpenOption;
import java.util.*;

public class DC1 {

    // <<regał, półka>, para>
    public TreeMap<Integer, List<Integer>> rackSet = new TreeMap<>();
    public List<String> shelves = new ArrayList<>();

    public static void main(String args[]) {
        DC1 dc1 = new DC1();
        dc1.fillSets();

        FileIO file = new FileIO("DC1.csv");
        file.delete();
        file.setContent(dc1.generateTable(), StandardOpenOption.CREATE_NEW,StandardOpenOption.WRITE);
    }

    public static List<Integer> setRack(Integer shelves, Integer span, Integer pair) {
        List<Integer> data = new ArrayList<>();
        data.add(shelves);
        data.add(span);
        data.add(pair);

        return data;
    }

    public void fillSets() {
        shelves.add("A");
        shelves.add("B");
        shelves.add("C");
        shelves.add("D");
        shelves.add("E");
        shelves.add("F");

        rackSet.put(1, setRack(3, 14, 1));
        rackSet.put(2, setRack(2, 14, 1));

        rackSet.put(3, setRack(3, 14, 2));

        rackSet.put(4, setRack(2, 14, 3));
        rackSet.put(5, setRack(2, 14, 3));

        rackSet.put(6, setRack(2, 11, 4));
        rackSet.put(7, setRack(2, 11, 4));

        rackSet.put(8, setRack(2, 11, 5));
        rackSet.put(9, setRack(2, 11, 5));

        rackSet.put(10, setRack(4, 11, 6));
        rackSet.put(11, setRack(4, 11, 6));

        rackSet.put(12, setRack(4, 11, 7));
        rackSet.put(13, setRack(4, 11, 7));

        rackSet.put(14, setRack(4, 11, 8));
        rackSet.put(15, setRack(4, 11, 8));

        rackSet.put(16, setRack(4, 11, 9));

        rackSet.put(17, setRack(4, 11, 10));
        rackSet.put(18, setRack(4, 11, 10));

        rackSet.put(19, setRack(4, 11, 11));
        rackSet.put(20, setRack(4, 11, 11));

        rackSet.put(21,setRack(4, 11, 12));
        rackSet.put(22,setRack(4, 11, 12));

        rackSet.put(23, setRack(4, 11, 13));
        rackSet.put(24, setRack(4, 11, 13));

        rackSet.put(25, setRack(2, 11, 14));
        rackSet.put(26, setRack(2, 11, 14));

        rackSet.put(27, setRack(2, 11, 15));

        rackSet.put(28, setRack(2, 11, 16));
        rackSet.put(29, setRack(2, 11, 16));

        rackSet.put(30, setRack(2, 11, 17));
        rackSet.put(31, setRack(2, 11, 17));

        rackSet.put(32, setRack(2, 11, 18));

        rackSet.put(33, setRack(2, 11, 19));
        rackSet.put(34, setRack(2, 11, 19));

        rackSet.put(35, setRack(2, 11, 20));
        rackSet.put(36, setRack(2, 11, 20));

        rackSet.put(37, setRack(2, 11, 21));

        rackSet.put(38, setRack(2, 11, 22));
        rackSet.put(39, setRack(2, 11, 22));

        rackSet.put(40, setRack(2, 11, 23));
    }

    public String generateTable() {
        String table = "MAGAZYN\tSTREFA\tREGAŁ\tPÓŁKA\tPRZĘSŁO\tWEJŚCIE\tPARA\n";

        for (Map.Entry<Integer, List<Integer>> entry : rackSet.entrySet()) {
            for (Integer r=0; r<entry.getValue().get(0); r++) {
                for (Integer s=0; s<(entry.getValue().get(1) == 11 ? entry.getValue().get(1)*4 : entry.getValue().get(1)*3); s++) {
                    table += String.format("DC1\tB0\tR%s\t%s\t%d\tW%s\tP%s\n",
                            entry.getKey() < 10 ? String.format("0%d", entry.getKey()) : entry.getKey(),
                            shelves.get(r),
                            s+1,
                            entry.getKey(),
                            entry.getValue().get(2));
                }
            }
        }

        return table;
    }
}
