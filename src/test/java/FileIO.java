import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.TreeMap;

public class FileIO extends File{
    private File file;
    private Charset charset = StandardCharsets.UTF_8;

    private TreeMap<Integer, String> fileContent = new TreeMap<>();

    public FileIO(String pathname) {
        super(pathname);
        file = new File(pathname);
        loadContent();
    }

    public void loadFile(String filename) {
        this.file = new File(filename);
        loadContent();
    }
    public File getFile() {
        return this.file;
    }
    public void setCharset(Charset charset) {
        this.charset = charset;
    }
    public Charset getCharset() {
        return this.charset;
    }
    public Path toPath() {
        return this.file.toPath();
    }
    public String getPath() {
        return this.file.getPath();
    }
    public String getFileName() {
        return this.file.getName();
    }
    public Integer getLineCount() {
        try {
            InputStream is = new BufferedInputStream(new FileInputStream(getFileName()));
            try {
                byte[] c = new byte[1024];

                int readChars = is.read(c);
                if (readChars == -1) {
                    // bail out if nothing to read
                    return 0;
                }

                // make it easy for the optimizer to tune this loop
                int count = 0;
                while (readChars == 1024) {
                    for (int i = 0; i < 1024; ) {
                        if (c[i++] == '\n') {
                            ++count;
                        }
                    }
                    readChars = is.read(c);
                }

                // count remaining characters
                while (readChars != -1) {
                    for (int i = 0; i < readChars; ++i) {
                        if (c[i] == '\n') {
                            ++count;
                        }
                    }
                    readChars = is.read(c);
                }

                return count == 0 ? 1 : count;
            }
            finally {
                is.close();
            }
        }
        catch (FileNotFoundException ex) {
            System.err.format("getLineCount -> FileNotFoundException: %s%n", ex);
        }
        catch (IOException ex) {
            System.err.format("getLineCount -> IOException: %s%n", ex);
        }

        return 0;
    }


    public void setContent(String content, StandardOpenOption... oo) {
        if (content.startsWith("\n") && getLineCount() == 0) {
            content = content.replace("\n", "");
        }

        byte bContent[] = content.getBytes(this.charset);

        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(this.file.toPath(), oo))) {
            out.write(bContent, 0, bContent.length);
        } catch (IOException x) {
            System.err.format("setContent -> IOException: %s%n", x);
        }

        loadContent();
    }
    public void loadContent() {
        fileContent = getContent();
    }
    public TreeMap<Integer, String> getContent() {
        TreeMap<Integer, String> lineContent = new TreeMap<>();

        try (BufferedReader reader = Files.newBufferedReader(this.file.toPath(), this.charset)) {
            String text;
            Integer line = 1;

            while ((text = reader.readLine()) != null) {
                lineContent.put(line, text);
                line++;
            }
        } catch (IOException ex) {
            System.err.format("loadContent -> IOException: %s%n", ex);
        }

        return lineContent;
    }
    public String getContentByLine(Integer line) {
        if (line > getLineCount()) {
            System.err.format("File has %d lines - %d requested", getLineCount(), line);
            return "";
        }

        return this.fileContent.get(line);
    }

    public void removeContentByLine(Integer line) {
        fileContent.remove(line);
        saveContentToFile();
    }

    public void removeLineByContent(String content) {
        for (int i=1; i<=getLineCount(); i++) {
            if (fileContent.get(i).equals(content)) {
                removeContentByLine(i);
                saveContentToFile();
                break;
            }
        }
    }

    public void saveContentToFile() {
        String content = "";

        for (int i=1; i<=fileContent.size(); i++) {
            content += i == fileContent.size() ? fileContent.get(i) : fileContent.get(i) + System.lineSeparator();
        }

        byte bContent[] = content.getBytes(this.charset);

        try (OutputStream out = new BufferedOutputStream(Files.newOutputStream(this.file.toPath()))) {
            out.write(bContent, 0, bContent.length);
        } catch (IOException x) {
            System.err.format("saveContentToFile -> IOException: %s%n", x);
        }

        loadContent();
    }

    public void clearEmptyLines() {
        TreeMap<Integer, String> lineContent = new TreeMap<>();
        Integer index = 1;

        for (int i=1; i<=fileContent.size(); i++) {
            if (fileContent.get(i).isBlank() ||
                    fileContent.get(i) == "" ||
                    fileContent.get(i) == null) {
                continue;
            }
            else {
                lineContent.put(index, fileContent.get(i));
                index++;
            }
        }

        fileContent = lineContent;
        saveContentToFile();
        loadContent();
    }
}