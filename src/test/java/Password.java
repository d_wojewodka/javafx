import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

public class Password {
    private String password = "";
    private String uniqueKey = null;

    public Password(String pwd) {
        this.password = pwd;
    }

    public static void main(String[] args) {
    }

    public void encryptPassword() {
        Integer requiredChar = this.password.length()%2 == 0 ? this.password.length()/2 : (this.password.length()/2)+1;
        SortedSet<Integer> temp = new TreeSet<Integer>();
        Random random = new Random();
        String pwd = "";
        Integer rnd;

        while (true) {
            if (temp.size() >= requiredChar) {
                break;
            }

            rnd = random.nextInt(this.password.length());
            if (!temp.contains(rnd)) {
                temp.add(rnd);
            }
        }

        this.uniqueKey = temp.toString().replace("[", "").replace("]", "").replace(", ", ":");
    }

    public boolean validatePassword(String pwd) {
        if (this.password.length() == 0 || pwd.length() == 0) {
            return false;
        }

        Integer requiredChar = this.password.length()%2 == 0 ? this.password.length()/2 : (this.password.length()/2)+1;
        String[] temp = this.uniqueKey.split(":");


        for (int i=0; i< temp.length; i++) {
            Integer start = Integer.parseInt(temp[i]);
            if (this.password.charAt(start) != pwd.charAt(start)) {
                return false;
            }
        }

        return true;
    }

}