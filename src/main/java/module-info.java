module org.test {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.test to javafx.fxml;
    exports org.test;
}