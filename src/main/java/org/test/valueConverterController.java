package org.test;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.io.IOException;

public class valueConverterController {
    @FXML private TextField bar, psi, km, miles, kw, hp;
    @FXML public void initialize() {}

    public valueConverterController() {
        System.err.format("constructor -> valueConverterController.java%n");
    }

    @FXML public void calBARPSI() {
        if (psi.getText().length() > 0) {
            bar.setText(Double.toString(Double.parseDouble(psi.getText())*0.0689475729));
        }
        else if (bar.getText().length() > 0) {
            psi.setText(Double.toString(Double.parseDouble(bar.getText())*14.5037738));
        }
    }

    @FXML public void calKMMILES() {
        if (km.getText().length() > 0) {
            miles.setText(Double.toString(Double.parseDouble(km.getText())*0.621371192));
        }
        else if (miles.getText().length() > 0) {
            km.setText(Double.toString(Double.parseDouble(miles.getText())*1.609344));
        }
    }

    @FXML public void calKWHP() {
        if (kw.getText().length() > 0) {
            hp.setText(Double.toString(Double.parseDouble(kw.getText())*1.34102209));
        }
        else if (hp.getText().length() > 0) {
            kw.setText(Double.toString(Double.parseDouble(hp.getText())*0.745699872));
        }
    }

    @FXML private void switchToMain() throws IOException {
        App.switchScene(sceneSet.MAIN);
    }
}
