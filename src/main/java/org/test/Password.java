package org.test;

import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

public class Password {
    private String password = "";
    private String uniqueKey = null;

    public Password(String pwd) {
        this.password = pwd;
    }

    public void encryptPassword() {
        Integer requiredChar = password.length()%2 == 0 ? password.length()/2 : (password.length()/2)+1;
        SortedSet<Integer> temp = new TreeSet<Integer>();
        Random random = new Random();
        String pwd = "";
        Integer rnd;

        while (true) {
            if (temp.size() >= requiredChar) {
                break;
            }

            rnd = random.nextInt(password.length());
            if (!temp.contains(rnd)) {
                temp.add(rnd);
            }
        }

        uniqueKey = temp.toString().replace("[", "").replace("]", "").replace(", ", ":");
    }

    public boolean validatePassword(String pwd) {
        if (password.length() == 0 || pwd.length() == 0) {
            return false;
        }

        Integer requiredChar = password.length()%2 == 0 ? password.length()/2 : (password.length()/2)+1;
        String[] temp = uniqueKey.split(":");


        for (int i=0; i< temp.length; i++) {
            Integer start = Integer.parseInt(temp[i]);
            if (password.charAt(start) != pwd.charAt(start)) {
                return false;
            }
        }

        return true;
    }

}
