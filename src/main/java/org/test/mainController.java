package org.test;

import javafx.fxml.FXML;
import java.io.IOException;

public class mainController {

    public mainController() {
        System.err.format("constructor -> mainController.java%n");
    }

    @FXML public void initialize() {}

    @FXML private void switchToNext() throws IOException {
        App.switchScene(sceneSet.NEXT);
    }

    @FXML private void switchToValueConverter() throws IOException {
        App.switchScene(sceneSet.VALUECONVERTER);
    }

    @FXML public void switchToAutoComplete() throws IOException {
        App.switchScene(sceneSet.AUTOCOMPLETE);
    }

    @FXML public void switchToPasswordCredentials() throws IOException {
        App.switchScene(sceneSet.PASSWORDCREDENTIALS);
    }
}
