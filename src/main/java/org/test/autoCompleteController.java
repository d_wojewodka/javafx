package org.test;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.*;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;

import static java.lang.String.format;
import static java.nio.file.StandardOpenOption.*;

public class autoCompleteController {
    private static SortedSet<String> words = new TreeSet<String>();
    private static Path path;
    private static Charset charset;
    private static FileInOut file;

    @FXML private TreeView<String> treeView;
    @FXML private TextField input;
    @FXML private Label prediction;

    public autoCompleteController() {
        System.err.format("constructor -> nextController.java%n");
    }

    @FXML public void initialize() throws IOException {
        charset = Charset.forName("UTF-8");

        file = new FileInOut("WordList.ini");
        path = file.toPath();

        //file.clearEmptyLines();
        loadWordList();
        fillList();

        App.getScene().setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.ENTER) {
                if (input.getText().length() != 0) {
                    autoComplete();
                }
            }
        });
    }

    private void loadWordList() {
        SortedMap<Integer, String> lineContent = file.getFileContent();
        for (int i=1; i<=lineContent.size(); i++) {
            words.add(lineContent.get(i));
        }
    }

    @FXML private String mergeString(String start, String end) {
        if (!start.toLowerCase().matches(end.toLowerCase())) {
            return start + end.substring(start.length());
        }
        else {
            return start;
        }
    }

    @FXML private void onInputChange() {
        String inp = input.getText().toLowerCase();

        if (input.getText().length() > 0) {
            if (words.size() == 0) {
                prediction.setText("");
                return;
            }

            Iterator<String> it = words.iterator();

            while(it.hasNext()) {
                String str = it.next();

                if (str.toLowerCase().startsWith(inp)) {
                    prediction.setText(mergeString(input.getText(), str));
                    if (inp.toLowerCase().matches(str.toLowerCase())) {
                        continue;
                    }
                    else {
                        break;
                    }
                }
                else {
                    prediction.setText("");
                }
            }
        }
        else {
            prediction.setText("Enter text here...");
        }
    }

    @FXML private void autoComplete() {
        if (prediction.getText().length() > 0) {
            input.setText(prediction.getText());
            input.positionCaret(prediction.getText().length());
        }
    }

    @FXML private void addToWordlist() {
        if (input.getText().contains(" ")) {
            System.err.format("This is not a word because it contains space!%n");
            return;
        }

        if (!words.contains(input.getText()) && input.getText().length() != 0) {
            words.add(input.getText());
            file.writeToFile("\n" + input.getText(), WRITE, APPEND);

            input.clear();
            prediction.setText("Enter text here...");
            fillList();
        }
        else {
            if (input.getText().length() == 0)
                System.err.format("Input is empty%n", input.getText());
            else
                System.err.format("%s is already in wordlist%n", input.getText());
        }

        //file.saveContentToFile();
    }

    @FXML public void clearWordList() {
        file.writeToFile("", WRITE, TRUNCATE_EXISTING);

        prediction.setText("Enter text here...");
        words.clear();
        fillList();
    }

    @FXML public void fillList() {
        file.loadFileContent();

        Iterator<String> it = words.iterator();
        TreeItem<String> root = new TreeItem("root");
        TreeItem<String> tree;

        if (words.size() != 0)
        {
            root.setValue(format("Word list - %d words", words.size()));
            root.setExpanded(true);
        }
        else {
            root.setValue("Word list is empty");
        }

        treeView.setRoot(root);

        while (it.hasNext()) {
            String str = it.next();
            tree = new TreeItem("+ " + str);
            root.getChildren().addAll(tree);
        }
    }

    @FXML public void switchToMain() throws IOException {
        App.switchScene(sceneSet.MAIN);
    }
}
