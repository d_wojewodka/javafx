package org.test;

import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;

import static java.nio.file.StandardOpenOption.*;

public class passwordCredentialsController {
    @FXML private TextField lengthTextField, countTextField, lowerTextField, upperTextField, numberTextField, specialTextField;
    @FXML private CheckBox lowerCheckBox, upperCheckBox, numberCheckBox, specialCheckBox, repeatCaseCheckBox, repeatCheckBox, leakCheckBox;
    @FXML private Slider lowerSlider, upperSlider, numberSlider, specialSlider;
    @FXML private Label combinationStatusLabel, lowerStatusLabel, upperStatusLabel, numberStatusLabel, specialStatusLabel;
    @FXML private TextArea characterTextArea;
    @FXML private TreeView<String> passwordTreeView;

    private FileInOut file;

    private List CHARACTERS, PASSWORDS;
    private List tempLowerCase, tempUpperCase, tempSpecial, tempNumber;

    @FXML public void initialize() {
        TreeItem rootItem = new TreeItem();
        passwordTreeView.setRoot(rootItem);
        passwordTreeView.setShowRoot(false);

        CHARACTERS = new ArrayList();
        PASSWORDS = new ArrayList();
        tempLowerCase = new ArrayList();
        tempUpperCase = new ArrayList();
        tempSpecial = new ArrayList();
        tempNumber = new ArrayList();

        try {
            file = new FileInOut("passwordList.txt");
            //fileContent = file.getFileContent();
        }
        catch (IOException ex) {
            System.err.println("error: " + ex);
        }
    }

    @FXML public void onToggleLowerCase() {
        lowerTextField.setText(lowerCheckBox.isSelected() ? lowerTextField.getPromptText() : "");
        onCharacterInputUpdate();
    }
    @FXML public void onToggleUpperCase() {
        upperTextField.setText(upperCheckBox.isSelected() ? upperTextField.getPromptText() : "");
        onCharacterInputUpdate();
    }
    @FXML public void onToggleNumber() {
        numberTextField.setText(numberCheckBox.isSelected() ? numberTextField.getPromptText() : "");
        onCharacterInputUpdate();
    }
    @FXML public void onToggleSpecial() {
        specialTextField.setText(specialCheckBox.isSelected() ? specialTextField.getPromptText() : "");
        onCharacterInputUpdate();
    }

    @FXML public void onClickGenerateButton() throws IOException {
        String error_message = "";

        passwordTreeView.setRoot(new TreeItem("root"));

        if (!lowerCheckBox.isSelected() & !upperCheckBox.isSelected() && !specialCheckBox.isSelected() && !numberCheckBox.isSelected()) {
            error_message = "-> Select at least one checkbox";
            passwordTreeView.getRoot().getChildren().addAll(new TreeItem(error_message));
        }
        if (lengthTextField.getText() == "") {
            error_message = "-> Enter password length";
            passwordTreeView.getRoot().getChildren().addAll(new TreeItem(error_message));
        }
        if (countTextField.getText() == "") {
            error_message = "-> Enter password count";
            passwordTreeView.getRoot().getChildren().addAll(new TreeItem(error_message));
        }
        if (error_message.length() > 0) {
            return;
        }

        PASSWORDS = new ArrayList();
        CHARACTERS = new ArrayList();

        calculateCharacters(lowerSlider, lowerTextField, tempLowerCase);
        calculateCharacters(upperSlider, upperTextField, tempUpperCase);
        calculateCharacters(numberSlider, numberTextField, tempNumber);
        calculateCharacters(specialSlider, specialTextField, tempSpecial);

        passwordTreeView.setRoot(new TreeItem("root"));

        for (Integer i=1; i<Integer.parseInt(countTextField.getText())+1; i++) {
            while (true) {
                String password = createPassword(CHARACTERS,
                        Integer.parseInt(lengthTextField.getText()),
                        repeatCaseCheckBox.isSelected() ? true : false,
                        repeatCheckBox.isSelected() ? true : false);

                if (!PASSWORDS.contains(password)) {
                    if (leakCheckBox.isSelected()) {
                        password = countOccurrencesV5(file.toPath(), password) > 0 ? "[LEAKED] " + password : password;
                    }
                    passwordTreeView.getRoot().getChildren().addAll(new TreeItem(password));
                    PASSWORDS.add(password);
                    break;
                }
            }
        }
    }
    @FXML public void onClickSaveButton() {
        for (int i=0; i<PASSWORDS.size(); i++) {
            file.writeToFile((String) PASSWORDS.get(i), APPEND, WRITE, CREATE);
        }
    }
    @FXML public void onClickKeyboard() {
        if (lengthTextField.getText().length() > 0) {
            combinationStatusLabel.setText(String.format("possible combinations: %.0f", Math.pow(Double.valueOf(lengthTextField.getText()), Double.parseDouble(String.valueOf(tempLowerCase.size()+tempUpperCase.size()+tempSpecial.size()+tempNumber.size())))));
        }
    }
    @FXML public void onCharacterInputUpdate() {
        lowerCheckBox.setSelected(lowerTextField.getText().length() > 0 ? true : false);
        upperCheckBox.setSelected(upperTextField.getText().length() > 0 ? true : false);
        numberCheckBox.setSelected(numberTextField.getText().length() > 0 ? true : false);
        specialCheckBox.setSelected(specialTextField.getText().length() > 0 ? true : false);

        characterTextArea.setText(lowerTextField.getText()
                + upperTextField.getText()
                + numberTextField.getText()
                + specialTextField.getText());
    }
    @FXML public void onUpdate() {
        toggleCheckBox(lowerCheckBox, lowerTextField);
        toggleCheckBox(upperCheckBox, upperTextField);
        toggleCheckBox(numberCheckBox, numberTextField);
        toggleCheckBox(specialCheckBox, specialTextField);
        refreshStatus(lowerSlider, lowerStatusLabel);
        refreshStatus(upperSlider, upperStatusLabel);
        refreshStatus(numberSlider, numberStatusLabel);
        refreshStatus(specialSlider, specialStatusLabel);
    }

    @FXML public void toggleCheckBox(CheckBox box, TextField input) {
        if (input.isFocused()) {
            if (input.getText().length() > 0) {
                box.setSelected(true);
            } else if (input.getText().length() == 0) {
                box.setSelected(true);
            }

            onCharacterInputUpdate();
        }
    }

    public static long countOccurrencesV5(Path path, String text) throws IOException {
        final long MAP_SIZE = 5242880; // 5 MB in bytes

        if (path == null || text == null) {
            throw new IllegalArgumentException("Path/text cannot be null");
        }

        if (text.isBlank()) {
            return 0;
        }

        final byte[] texttofind = text.getBytes(StandardCharsets.UTF_8);

        long count = 0;
        try (FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.READ)) {

            long position = 0;
            long length = fileChannel.size();
            while (position < length) {

                long remaining = length - position;
                long bytestomap = (long) Math.min(MAP_SIZE, remaining);
                MappedByteBuffer mbBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, position, bytestomap);

                long limit = mbBuffer.limit();
                long lastSpace = -1;
                long firstChar = -1;
                while (mbBuffer.hasRemaining()) {

                    boolean isFirstChar = false;
                    while (firstChar != 0 && mbBuffer.hasRemaining()) {

                        byte currentByte = mbBuffer.get();

                        if (Character.isWhitespace((char) currentByte)) {
                            lastSpace = mbBuffer.position();
                        }

                        if (texttofind[0] == currentByte) {
                            isFirstChar = true;
                            break;
                        }
                    }

                    if (isFirstChar) {

                        boolean isRestOfChars = true;

                        int j;
                        for (j = 1; j < texttofind.length; j++) {
                            if (!mbBuffer.hasRemaining() || texttofind[j] != mbBuffer.get()) {
                                isRestOfChars = false;
                                break;
                            }
                        }

                        if (isRestOfChars) {
                            count++;
                            lastSpace = -1;
                        }

                        firstChar = -1;
                    }
                }

                if (lastSpace > -1) {
                    position = position - (limit - lastSpace);
                }

                position += bytestomap;
            }
        }

        return count;
    }

    @FXML public String checkPassword(String pwd) throws IOException {
        return "";
    }

    @FXML public void calculateCharacters(Slider slider, TextField input, List characters) {
        characters.clear();

        for (Object object: input.getText().toCharArray()) {
            characters.add(object);
            for (Integer i=0; i<slider.getValue(); i++) {
                CHARACTERS.add(object);
            }
        }
    }
    @FXML public void refreshStatus(Slider slider, Label status) {
        if (slider.isPressed()) {
            status.setText(String.format("%.0f%%", slider.getValue()));
        }
    }

    @FXML private String createPassword(List character_set, Integer password_length, boolean no_repeat_sign, boolean no_repeat_case) {
       Character[] password = new Character[password_length];

        for (Integer i = 0; i < password_length; i++) {
            while (true) {
                Character ch = (Character) character_set.get(new Random().nextInt(character_set.size()));

                if (i != 0 && (no_repeat_sign != false || no_repeat_case != false)) {
                    if (no_repeat_sign) {
                        if (ch == password[i - 1]) {
                            continue;
                        }
                    }
                    if (no_repeat_case) {
                        if (Character.toUpperCase(ch) == Character.toUpperCase(password[i - 1]) && Character.toLowerCase(ch) == Character.toLowerCase(password[i - 1])) {
                            continue;
                        }
                    }

                }

                password[i] = ch;
                break;
            }
        }

        return Arrays.toString(password).replaceAll("\\[|\\]|,|\\s", "");
    }

    @FXML private void switchToMain() throws IOException {
        App.switchScene(sceneSet.MAIN);
    }
}
