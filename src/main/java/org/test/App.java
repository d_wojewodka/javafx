package org.test;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

enum sceneSet {
    MAIN                        { public String toString() { if (App.getStage() != null) { App.setSceneSize(535.0, 400.0); }return "main"; }},
    NEXT                        { public String toString() { if (App.getStage() != null) { App.setSceneSize(540.0, 180.0); }return "next"; }},
    VALUECONVERTER              { public String toString() { if (App.getStage() != null) { App.setSceneSize(422.0, 155.0); }return "valueConverter"; }},
    AUTOCOMPLETE                { public String toString() { if (App.getStage() != null) { App.setSceneSize(415.0, 440.0); }return "autoComplete"; }},
    PASSWORDCREDENTIALS         { public String toString() { if (App.getStage() != null) { App.setSceneSize(730.0, 585.0); }return "passwordCredentials"; }},
}

public class App extends Application {
    private static Scene scene;
    private static Stage stage;

    @Override public void start(Stage st) throws IOException {
        scene = new Scene(loadFXML(sceneSet.MAIN));
        st.setScene(scene);

        stage = st;
        stage.setTitle(sceneSet.MAIN.toString());
        stage.setResizable(false);
        stage.show();
    }

    public static Scene getScene() { return scene; }
    public static Stage getStage() { return stage; }

    public static void setSceneSize(Double x, Double y) {
        stage.setHeight(y);
        stage.setWidth(x);
    }

    static void switchScene(sceneSet select) throws IOException {
        scene.setRoot(loadFXML(select));
        stage.setTitle(select.name());
    }

    private static Parent loadFXML(sceneSet select) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource((select.toString() + ".fxml")));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

}