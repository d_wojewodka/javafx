package org.test;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.util.*;

public class nextController {
    private Integer selectedFieldId;

    @FXML private TextField i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, password;
    @FXML private Label dialog;
    @FXML private Button check;

    private String pwd;
    private SortedSet<Integer> key;


    @FXML public void initialize() {
        i1.requestFocus();
        selectedFieldId = Integer.getInteger(i1.getId().substring(1));
        check.setVisible(false);

        for (int i=1; i<=12; i++) {
            getFieldBySlot(i).setVisible(false);
        }
    }

    public nextController() {
        System.err.format("constructor -> nextController.java%n");
    }

    @FXML public TextField getFieldBySlot(Integer id) {
        switch (id) {
            case 1: return i1;
            case 2: return i2;
            case 3: return i3;
            case 4: return i4;
            case 5: return i5;
            case 6: return i6;
            case 7: return i7;
            case 8: return i8;
            case 9: return i9;
            case 10: return i10;
            case 11: return i11;
            case 12: return i12;
        }

        return null;
    }

    @FXML public void onUpdateInput() {
        if (getFieldBySlot(selectedFieldId).getText().length() > 0) {
            if (selectedFieldId < 12)
                getFieldBySlot(selectedFieldId+1).requestFocus();
        }
    }

    @FXML public void clickGenerateKey() {
        if (password.getText().length() == 0) {
            return;
        }

        key = generateKey(password.getText());
        pwd = password.getText();
        check.setVisible(true);

        for (int i=1; i<=12; i++) {
            if (i > password.getText().length())
                getFieldBySlot(i).setVisible(false);
            else if (!key.contains(i)) {
                getFieldBySlot(i).setVisible(true);
                getFieldBySlot(i).setDisable(true);
                getFieldBySlot(i).setText("*");
            }
            else {
                getFieldBySlot(i).setVisible(true);
                getFieldBySlot(i).setDisable(false);
                getFieldBySlot(i).setText("");
            }
        }
    }

    @FXML public SortedSet<Integer> generateKey(String password) {
        key = new TreeSet<Integer>();
        Random random = new Random();
        Integer rnd;

        while (true) {
            if (key.size() >= password.length()/2) {
                break;
            }

            rnd = random.nextInt(password.length())+1;
            if (!key.contains(rnd)) {
                key.add(rnd);
            }
        }

        showEncryptedPassword(key, password);

        return key;
    }

    @FXML public void showEncryptedPassword(SortedSet<Integer> key, String password) {
        String pwd = "";

        for (int i=1; i<=password.length(); i++) {
            if (key.contains(i)) {
                pwd += "*";
            }
            else {
                pwd += password.substring(i-1, i);
            }
        }

        System.out.format("Original password  %s%n", password);
        System.out.format("Encrypted password %s%n", pwd);
    }

    @FXML public void validate() {
        for (Integer object : key) {
            if (!getFieldBySlot(object).getText().equals(Character.toString(pwd.charAt(object-1)))) {
                dialog.setText("Mismatch!");
                dialog.setTextFill(Color.RED);
                return;
            }
        }

        dialog.setText("Correct!");
        dialog.setTextFill(Color.GREEN);
    }

    @FXML private void switchToMain() throws IOException {
        App.switchScene(sceneSet.MAIN);
    }
}